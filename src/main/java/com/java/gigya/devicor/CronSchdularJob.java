package com.java.gigya.devicor;

import java.util.Date;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java.devicor.jsontocsv.Main;
import com.java.devicor.util.DevicorUtil;


/**
 * 
 * @author VSEERLA
 *
 */
public class CronSchdularJob {

	private final static Logger logger = LoggerFactory.getLogger(CronSchdularJob.class);
	
	public static void main(String[] args) throws SchedulerException {

		logger.info("###  MainClass.main() START ###");

		StringBuilder sb = new StringBuilder();
		String cronExpression = null;
		
//		String args1[]= new String[3];
//		args1[0] ="0";
//		args1[1]="0";
//		args1[2]="12";
//		String args2[] = {"0","0","12"};
		
		if (args != null && args.length == 3 ) {
			String seconds = args[0];
			String minutes = args[1];
			String hours = args[2];

			if (seconds != null && minutes != null && hours != null) {
				cronExpression = sb.append(seconds).append(" ").append(minutes).append(" ").append(hours).append(" ")
						.append("*").append(" ").append("*").append(" ").append("?").toString();
				logger.info("### CRON EXPRESSION ===> " + cronExpression);
			}
		}
		try {

			// Set job details.
			JobDetail job = JobBuilder.newJob(Main.class).withIdentity("main", "devicor").build();

			// Set the scheduler timings.
			Trigger trigger = TriggerBuilder.newTrigger().withIdentity("cronTrigger", "devicor")
//					.withSchedule(CronScheduleBuilder.cronSchedule("30 * * * * ?")).build();
					.withSchedule(CronScheduleBuilder.cronSchedule(cronExpression != null ? cronExpression:"* * 4 * * ?")).build();

			TriggerKey key = trigger.getKey();
			logger.info("trigger Key " + key);

			// Execute the job.
			Scheduler scheduler = new StdSchedulerFactory().getScheduler();

			DevicorUtil devicorUtil = new DevicorUtil();
			devicorUtil.getCurrentDateTime();

			Date scheduleJob = scheduler.scheduleJob(job, trigger);
			logger.info("### SCHDULING JOB IS : " + scheduleJob);
			scheduler.start();

		} catch (Exception e) {
			logger.error("### EXCEPTION OCCURED ===> "+e.getMessage());
		}

		logger.info("###  MainClass.main()  END ###");
	}

}
