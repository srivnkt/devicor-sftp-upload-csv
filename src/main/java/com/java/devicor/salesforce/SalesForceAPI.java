package com.java.devicor.salesforce;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java.devicor.constants.IMammotome;
import com.java.devicor.jsontocsv.parser.JSONFlattener;
import com.java.devicor.util.DevicorUtil;
/**
 * 
 * @author
 * 
 *  This class is used for fetching the user consent data from Salesforce database.
 *
 */
public class SalesForceAPI {
	
	private final static Logger logger = LoggerFactory.getLogger(SalesForceAPI.class);

	private static Header AuthHeader;
	static String ACCESS_TOKEN;
	static String INSTANCE_URL;
//	static final String DATA_SERVICE = "/services/data/v46.0/query";
//	static final String DATA_SERVICE1 = "/services/data/v46.0/sobjects/Lead/deleted/​​​";
//	static final String DATA_SERVICE1 = "/services/data/v46.0/queryAll";
	String PARAMS = "?q=";
	
	/**
	 * 
	 */
	public static void getAccessToken() {
		
		logger.info("### SalesForceAPI.getAccessToken() :: START");
		DevicorUtil devicorUtil = new DevicorUtil();
		Properties prop = devicorUtil.loadPropertiesFile("application.properties");
		HttpClient httpclient = HttpClientBuilder.create().build();

		// Assemble the login request URL
		String loginURL = prop.getProperty("LOGINURL") + IMammotome.TOKEN_SERVICE + "&client_id=" + IMammotome.CLIENTID + "&client_secret=" + IMammotome.CLIENTSECRET
				+ "&username=" + prop.getProperty("USERNAME") + "&password=" + prop.getProperty("PASSWORD");
		
		// Login requests must be POSTs
		HttpPost httpPost = new HttpPost(loginURL);
		HttpResponse response = null;
		String getResult = null;
		JSONObject jsonObject = null;
		
		try {
			// Execute the login POST request
			response = httpclient.execute(httpPost);
			// verify response is HTTP OK
			final int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode != HttpStatus.SC_OK) {
				logger.info("### ERROR AUTHENTICATING TO FORCE.COM: " + statusCode);
				return;
			}
			getResult = EntityUtils.toString(response.getEntity());
			jsonObject = (JSONObject) new JSONTokener(getResult).nextValue();
			ACCESS_TOKEN = jsonObject.getString("access_token");
			INSTANCE_URL = jsonObject.getString("instance_url");
			AuthHeader = new BasicHeader("Authorization", "Bearer " + ACCESS_TOKEN);
			
		} catch (Exception exception) {
			
			logger.error("### EXCEPTION OCCURED "+exception.getLocalizedMessage());
			logger.error("### EXCEPTION OCCURED "+exception.getMessage());
		}
		
		logger.info("### StatusLine "+response.getStatusLine());
		logger.info("### SUCCESSFUL LOGIN");
		logger.info("###  INSTANCE URL ===> : " + INSTANCE_URL);
		logger.info("###  ACCESS TOKEN  ===> : " + ACCESS_TOKEN);
		// release connection
		httpPost.releaseConnection();
		
		logger.info("### SalesForceAPI.getAccessToken() :: END");
		
	}
		
	/**
	 * 
	 * @param query
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public List<Map<String, String>> getRecords(String country, String... query) throws UnsupportedEncodingException {
		
		String PARAMS = "?q=";
			if(query.length >0) {
				PARAMS = PARAMS+ query[0];
			}else {
				PARAMS = PARAMS+ IMammotome.SALESFORCE_QUERY;
			}
			return queryRecords(PARAMS, country);
		}
	
	/**
	 * 
	 * @param query
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public List<Map<String, String>> getDeletedRecords(String country, String... query) throws UnsupportedEncodingException {
		
		String PARAMS = "?q=";
			if(query.length >0) {
				PARAMS = PARAMS+ query[0];
			}else {
				PARAMS = PARAMS+ IMammotome.SALESFORCE_QUERY_DELETED_RECORDS;
			}
			return queryForDeletedRecords(PARAMS, country);
		}
	
		
	/**
	 * 
	 * @param PARAMS
	 * @return
	 */
	public List<Map<String, String>> queryRecords(String PARAMS, String country) {
		
		getAccessToken();
		DevicorUtil devicorUtil = new DevicorUtil();
		Properties prop = devicorUtil.loadPropertiesFile("application.properties");
		List<Map<String, String>> flatJson = null;
		List<Map<String, String>> convertedflatJson = null;
		
		convertedflatJson = new ArrayList<Map<String,String>>();
		Map<String,String> m = null;
		
		logger.info("\n_______________ LEAD QUERY _______________");
		
		try {
		
			HttpClient httpClient = HttpClientBuilder.create().build();
			String encode = null;
			String uri = null;
			String data_service = prop.getProperty("DATA_SERVICE");
			
			if("United States".equals(country)) {
						
				encode = URLEncoder.encode(DevicorUtil.getUSCountryCode(country),"UTF-8");
				uri = INSTANCE_URL + data_service + PARAMS.concat(encode);
				
			}else if("China".equals(country)) {
				
				encode = URLEncoder.encode(DevicorUtil.getCNCountryCode(country),"UTF-8");
				uri = INSTANCE_URL + data_service + PARAMS.concat(encode);
				
			}else if("Russia".equals(country)) {
				
				encode = URLEncoder.encode(DevicorUtil.getRUCountryCode(country),"UTF-8");
				uri = INSTANCE_URL + data_service + PARAMS.concat(encode);
				
			}else if("Australia".equals(country) || "Korea".equals(country) || "Japan".equals(country)) {
				
				encode = URLEncoder.encode(DevicorUtil.getAUCountryCode(country),"UTF-8");
				uri = INSTANCE_URL + data_service + PARAMS.concat(encode);
				
			}else if("Europe".equals(country) || "Germany".equals(country) || "UK".equals(country) || "France".equals(country) || "Italy".equals(country)) {
				
				encode = URLEncoder.encode(DevicorUtil.getEUCountryCode(country),"UTF-8");
				uri = INSTANCE_URL + data_service + IMammotome.EUROPE_PARAMS.concat(encode);
			}

			logger.info("### QUERY STRING URL : " + uri);
			if (uri != null) {
				
				HttpGet httpGet = new HttpGet(uri);
				httpGet.addHeader(AuthHeader);
				// Make the request.
				HttpResponse response = httpClient.execute(httpGet);
				
				// Process the result
				int statusCode = response != null ? response.getStatusLine().getStatusCode():0;
				if (statusCode == 200) {
					String response_string = EntityUtils.toString(response.getEntity());
					try {
						JSONObject json = new JSONObject(response_string);
						flatJson = JSONFlattener.parseJson(json.getJSONArray("records").toString());
					} catch (JSONException jsonException) {
						logger.error("### EXCEPTION OCCURED " + jsonException.getLocalizedMessage());
						logger.error("### EXCEPTION OCCURED " + jsonException.getMessage());
					}
				} else {
					logger.info("### AN ERROR HAS OCCURED . HTTP STATUS: " + response.getStatusLine().getStatusCode());
				}
			}
			
			if (flatJson != null) {
				for (Iterator<Map<String, String>> iterator = flatJson.iterator(); iterator.hasNext();) {
					Map<String, String> map = (Map<String, String>) iterator.next();
					m = new HashMap<String, String>();
					
						m.put("Email", map.get("Email"));
						m.put("FirstName", map.get("FirstName"));
						m.put("LastName", map.get("LastName"));
						m.put("Id", map.get("Id"));
						m.put("Email_Opt_in__c", map.get("Email_Opt_in__c"));
						m.put("Phone_Opt_In__c", map.get("Phone_Opt_In__c"));
						m.put("Text_Opt_In__c", map.get("Text_Opt_In__c"));
						m.put("Country__c", map.get("Country__c"));
						convertedflatJson.add(m);
				}
			}

		} catch (Exception exception) {
			logger.error("### EXCEPTION OCCURED "+exception.getLocalizedMessage());
			logger.error("### EXCEPTION OCCURED "+exception.getMessage());
			exception.printStackTrace();
		}
		return convertedflatJson;
	}
	
	
	/**
	 * 
	 * @param PARAMS
	 * @return
	 */
	public List<Map<String, String>> queryForDeletedRecords(String PARAMS, String country) {
		
		getAccessToken();
		DevicorUtil devicorUtil = new DevicorUtil();
		Properties prop = devicorUtil.loadPropertiesFile("application.properties");
		String data_service1 = prop.getProperty("DATA_SERVICE1");
		List<Map<String, String>> flatJson = null;
		List<Map<String, String>> convertedflatJson = null;
		
		convertedflatJson = new ArrayList<Map<String,String>>();
		Map<String,String> m = null;
		
		logger.info("\n_______________ LEAD QUERY _______________");
		
		try {
			
			HttpClient httpClient = HttpClientBuilder.create().build();
			String encode = null;
			String uri = null;
			
			if("United States".equals(country)) {
				
//				String lastModifiedDate = devicorUtil.getLastModifiedDate();
						
				encode = URLEncoder.encode(DevicorUtil.getUSCountryCode1(country)+"AND LastModifiedDate >="+DevicorUtil.getLastModifiedDate()+ " AND IsDeleted=TRUE","UTF-8");
				uri = INSTANCE_URL + data_service1 + PARAMS.concat(encode);
											
			}else if("China".equals(country)) {
				
				encode = URLEncoder.encode(DevicorUtil.getCNCountryCode1(country)+"AND LastModifiedDate >="+DevicorUtil.getLastModifiedDate()+ " AND IsDeleted=TRUE","UTF-8");
				uri = INSTANCE_URL + data_service1 + PARAMS.concat(encode);
				
			}else if("Russia".equals(country)) {
				
				encode = URLEncoder.encode(DevicorUtil.getRUCountryCode1(country)+"AND LastModifiedDate >="+DevicorUtil.getLastModifiedDate()+ " AND IsDeleted=TRUE","UTF-8");
				uri = INSTANCE_URL + data_service1 + PARAMS.concat(encode);
				
			}else if("Australia".equals(country) || "Korea".equals(country) || "Japan".equals(country)) {
				
				encode = URLEncoder.encode(DevicorUtil.getAUCountryCode1(country)+"AND LastModifiedDate >="+DevicorUtil.getLastModifiedDate()+ " AND IsDeleted=TRUE","UTF-8");
				uri = INSTANCE_URL + data_service1 + PARAMS.concat(encode);
				
			}else if("Europe".equals(country) || "Germany".equals(country) || "UK".equals(country) || "France".equals(country) || "Italy".equals(country)) {
				
				encode = URLEncoder.encode(DevicorUtil.getEUCountryCode1(country)+"AND LastModifiedDate >="+DevicorUtil.getLastModifiedDate()+ " AND IsDeleted=TRUE","UTF-8");
				uri = INSTANCE_URL + data_service1 + PARAMS.concat(encode);
	
			}

			logger.info("### QUERY STRING URL : " + uri);
			if (uri != null) {
				
				HttpGet httpGet = new HttpGet(uri);
				httpGet.addHeader(AuthHeader);
				// Make the request.
				HttpResponse response = httpClient.execute(httpGet);
				
				// Process the result
				int statusCode = response != null ? response.getStatusLine().getStatusCode():0;
				if (statusCode == 200) {
					String response_string = EntityUtils.toString(response.getEntity());
					try {
						JSONObject json = new JSONObject(response_string);
						flatJson = JSONFlattener.parseJson(json.getJSONArray("records").toString());
					} catch (JSONException jsonException) {
						logger.error("### EXCEPTION OCCURED " + jsonException.getLocalizedMessage());
						logger.error("### EXCEPTION OCCURED " + jsonException.getMessage());
					}
				} else {
					logger.info("### AN ERROR HAS OCCURED . HTTP STATUS: " + response.getStatusLine().getStatusCode());
					logger.info("### AN ERROR HAS OCCURED . HTTP STATUS: " + response.getStatusLine().getReasonPhrase());
				}
			}
			
			if (flatJson != null) {
				for (Iterator<Map<String, String>> iterator = flatJson.iterator(); iterator.hasNext();) {
					Map<String, String> map = (Map<String, String>) iterator.next();
					m = new HashMap<String, String>();
					
						m.put("Email", map.get("Email"));
						m.put("FirstName", map.get("FirstName"));
						m.put("LastName", map.get("LastName"));
						m.put("Id", map.get("Id"));
						m.put("IsDeleted", map.get("IsDeleted"));
						m.put("Country__c", map.get("Country__c"));
						convertedflatJson.add(m);
				}
			}

		} catch (Exception exception) {
			logger.error("### EXCEPTION OCCURED "+exception.getLocalizedMessage());
			logger.error("### EXCEPTION OCCURED "+exception.getMessage());
			exception.printStackTrace();
		}
		return convertedflatJson;
	}
}
