/*
 * Copyright 2012-2014 Dristhi software
 * Copyright 2015 Arkni Brahim
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package com.java.devicor.jsontocsv;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.java.devicor.constants.IMammotome;
import com.java.devicor.jsontocsv.writer.CSVWriter;
import com.java.devicor.salesforce.SalesForceAPI;
import com.java.devicor.util.DeleteDirectory;
import com.java.devicor.util.DevicorUtil;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

/**
 * 
 * @author VSEERLA
 *
 */
@DisallowConcurrentExecution
public class Main implements Job {

	private final static Logger logger = LoggerFactory.getLogger(Main.class);
	static int i = 0;

	public void execute(JobExecutionContext context) throws JobExecutionException {

		logger.info("### Main.execute()  START ###");

		DevicorUtil devicorUtil = new DevicorUtil();
		Properties prop = devicorUtil.loadPropertiesFile("application.properties");
		String devicor_key = prop.getProperty("DEVICOR_GIGYA_KEY");
		synchronized (context) {
			String[] COUNTRY_NAME = { "United States", "China", "Russia", "Europe", "Australia" };
			SalesForceAPI salesForceAPI = new SalesForceAPI();
			List<Map<String, String>> respJson = null;
			for (String country : COUNTRY_NAME) {

				try {
					respJson = salesForceAPI.getRecords(country, IMammotome.SALESFORCE_QUERY);

					if (respJson != null && respJson.size() > 0) {

						/*
						 * this file can be id_rsa or id_dsa based on which algorithm is used to create
						 * the key.
						 * String privateKey =
						 * "LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb3dJQkFBS0NBUUVBd0M5Z2J5ZndtbENMWFRzd0t
						 * KNTlUOFUrU3JyU0NxMDl1ZGFKZk91M3lzN25zZkhHCjBhRS 83RUUxalV3SmhNT
						 */
						File folder = new File(devicor_key);
						String privateKey = folder.getAbsolutePath();

						JSch jSch = new JSch();
						Session session = null;
						Channel channel = null;
						ChannelSftp channelSftp = null;
						File file = null;
						
						try {

							jSch.addIdentity(privateKey);
							logger.info("### Private Key Added.");
							session = jSch.getSession(prop.getProperty("sftp.user"), prop.getProperty("sftp.host"),
									Integer.parseInt(prop.getProperty("sftp.port")));
							logger.info("session created.");

							java.util.Properties config = new java.util.Properties();
							config.put("StrictHostKeyChecking", "no");
							session.setConfig(config);
							session.connect();
							channel = session.openChannel("sftp");
							channel.connect();
							logger.info("shell channel connected....");
							channelSftp = (ChannelSftp) channel;

							String generatedFile = "";

							LocalDateTime now = LocalDateTime.now();
							DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");

							for (Map<String, String> map : respJson) {

								country = map.get("Country__c");

								boolean searchUSCountryName = DevicorUtil.searchUSCountryName(country);
								boolean searchAUCountryName = DevicorUtil.searchAUCountryName(country);
								boolean searchEUCountryName = DevicorUtil.searchEUCountryName(country);
								
								if (searchUSCountryName) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.for.us"));
									break;
								} else if ("China".equals(country)) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.for.cn"));
									break;
								} else if ("Russia".equals(country)) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.for.ru"));
									break;
								} else if (searchAUCountryName) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.for.au"));
									break;
								} else if (searchEUCountryName) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.for.eu"));
									break;
								}

							}

							generatedFile = "files/" + generatedFile;
							logger.info("### GENERATED FILE ===> : " + generatedFile);

							if (respJson != null && respJson.size() > 0) {
								CSVWriter.writeToFile(CSVWriter.getCSV(respJson), generatedFile);
							}

							Thread.sleep(3000);
							file = new File(generatedFile);
							File filePath = file.getAbsoluteFile();
							logger.info(" ###  FILE NAME  =============> " + filePath.getName());
							logger.info(" ###  ABSOLUTEPATH =============> " + filePath);

							if (filePath.length() == 0) {

								logger.info("### FILE IS EMPTY ...");
							} else {
								String SFTPWORKINGDIR1 = prop.getProperty("SFTPWORKINGDIR1")+ filePath.getName();
								channelSftp.put(filePath.getPath(), SFTPWORKINGDIR1);
							}

						} catch (JSchException e) {
							logger.error(e.getMessage());
						} catch (SftpException e) {
							logger.error(e.getMessage());
						} catch (InterruptedException e) {
							logger.error(e.getMessage());
						} catch (Exception e) {
							logger.error(e.getMessage());
						} finally {

							if (channelSftp != null) {
								channelSftp.disconnect();
								channelSftp.exit();
							}

							if (channel != null) {
								channel.disconnect();
							}

							if (session != null) {
								session.disconnect();
							}

							DeleteDirectory deleteDirectory = new DeleteDirectory();
							try {
								deleteDirectory.deleteGeneratedFile(file);

							} catch (IOException e) {
								logger.error(e.getMessage());
							}
						}

					}

				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
			}

			for (String country : COUNTRY_NAME) {

				try {
					respJson = salesForceAPI.getDeletedRecords(country, IMammotome.SALESFORCE_QUERY_DELETED_RECORDS);

					if (respJson != null && respJson.size() > 0) {

						/*
						 * this file can be id_rsa or id_dsa based on which algorithm is used to create
						 * the key.
						 *
						 * String privateKey =
						 * "LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFb3dJQkFBS0NBUUVBd0M5Z2J5ZndtbENMWFRzd0t
						 * KNTlUOFUrU3JyU0NxMDl1ZGFKZk91M3lzN25zZkhHCjBhRS 83RUUxalV3SmhNT
						 */

						File folder = new File(devicor_key);
						String privateKey = folder.getAbsolutePath();

						JSch jSch = new JSch();
						Session session = null;
						Channel channel = null;
						ChannelSftp channelSftp = null;
						File file = null;
						
						try {

							jSch.addIdentity(privateKey);
							logger.info("### Private Key Added.");
							session = jSch.getSession(prop.getProperty("sftp.user"), prop.getProperty("sftp.host"),
									Integer.parseInt(prop.getProperty("sftp.port")));
							logger.info("session created.");

							java.util.Properties config = new java.util.Properties();
							config.put("StrictHostKeyChecking", "no");
							session.setConfig(config);
							session.connect();
							channel = session.openChannel("sftp");
							channel.connect();
							logger.info("shell channel connected....");
							channelSftp = (ChannelSftp) channel;

							String generatedFile = "";

							LocalDateTime now = LocalDateTime.now();
							DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");

							for (Map<String, String> map : respJson) {

								country = map.get("Country__c");

								boolean searchUSCountryName = DevicorUtil.searchUSCountryName(country);
								boolean searchAUCountryName = DevicorUtil.searchAUCountryName(country);
								boolean searchEUCountryName = DevicorUtil.searchEUCountryName(country);

								if (searchUSCountryName) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.deleted.for.us"));
									break;
								} else if ("China".equals(country)) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.deleted.for.cn"));
									break;
								} else if ("Russia".equals(country)) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.deleted.for.ru"));
									break;
								} else if (searchAUCountryName) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.deleted.for.au"));
									break;
								} else if (searchEUCountryName) {
									generatedFile = now.format(formatter)
											.concat(prop.getProperty("generateFile.deleted.for.eu"));
									break;
								}

							}

							generatedFile = "files/" + generatedFile;
							logger.info("### GENERATED FILE ===> : " + generatedFile);

							if (respJson != null && respJson.size() > 0) {
								CSVWriter.writeToFile(CSVWriter.getCSV(respJson), generatedFile);
							}

							Thread.sleep(3000);
							file = new File(generatedFile);
							File filePath = file.getAbsoluteFile();
							logger.info(" ###  FILE NAME  =============> " + filePath.getName());
							logger.info(" ###  ABSOLUTEPATH =============> " + filePath);

							if (filePath.length() == 0) {

								logger.info("### FILE IS EMPTY ...");
							} else {
								String SFTPWORKINGDIR1 = prop.getProperty("SFTPWORKINGDIR1")+ filePath.getName();
								channelSftp.put(filePath.getPath(), SFTPWORKINGDIR1);
							}

						} catch (JSchException e) {
							logger.error(e.getMessage());
						} catch (SftpException e) {
							logger.error(e.getMessage());
						} catch (InterruptedException e) {
							logger.error(e.getMessage());
						} catch (Exception e) {
							logger.error(e.getMessage());
						} finally {

							if (channelSftp != null) {
								channelSftp.disconnect();
								channelSftp.exit();
							}

							if (channel != null) {
								channel.disconnect();
							}

							if (session != null) {
								session.disconnect();
							}

							DeleteDirectory deleteDirectory = new DeleteDirectory();
							try {
								deleteDirectory.deleteGeneratedFile(file);

							} catch (IOException e) {
								logger.error(e.getMessage());
							}
						}
					}

				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
			}
		}
		
		updateDateTime();
		logger.info("### Main.execute()  END ###");
	}
	
	/**
	 * 
	 */
	public void updateDateTime() {

		logger.info("###  DevicorUtil.getCurrentDateTime() :: START");

		DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, 0);
		now.set(Calendar.HOUR, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.HOUR_OF_DAY, 0);

		LocalDateTime ldt = LocalDateTime.ofInstant(now.getTime().toInstant(), ZoneId.systemDefault());
		ZonedDateTime zonedDateTime = ZonedDateTime.of(ldt, ZoneId.of("UTC"));
		String currentDateTime = zonedDateTime.format(DATE_TIME_FORMATTER);
		logger.info("### CREATED DATE IS : " + currentDateTime);
		
		DevicorUtil devicorUtil = new DevicorUtil();
		Properties prop = devicorUtil.loadPropertiesFile("application.properties");
		String path = prop.getProperty("path");
		File directory = new File(path);
		
		if (directory.exists()) {

			try {
	            FileUtils.writeStringToFile(new File(path), currentDateTime, StandardCharsets.UTF_8);
	        } catch (IOException e) {
	           logger.info("### EXCEPTION OCCURED :: "+e.getMessage());
	        }
		}
		logger.info("###  DevicorUtil.getCurrentDateTime() :: END");
	}

}
