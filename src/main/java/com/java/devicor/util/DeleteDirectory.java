package com.java.devicor.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author VSEERLA
 *
 */
public class DeleteDirectory {

	private final static Logger logger = LoggerFactory.getLogger(DeleteDirectory.class);

	public void deleteGeneratedFile(File directory) throws IOException {

		InputStream inputStream = null;
		try {
			Properties prop = new Properties();
			String propFileName = "application.properties";

			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException(
						" ###  PROPERTY FILE ===>  '" + propFileName + "' NOT FOUND IN THE CALSSPATH");
			}

//			File directory = new File(prop.getProperty("delete.generatedFile"));

			// make sure directory exists
			if (!directory.exists()) {

				logger.info("Directory does not exist.");
//				System.exit(0);

			} else {

				try {
					
					synchronized (directory) {
						delete(directory);
					}

				} catch (IOException e) {
					e.printStackTrace();
					System.exit(0);
				}
			}

		} catch (Exception e) {
			logger.error("### EXCEPTION OCCURED : " + e);
		} finally {
			inputStream.close();
		}

		logger.info("Done");
	}

	public static void delete(File file) throws IOException {
		
		
		if (file.isDirectory()) {

			// directory is empty, then delete it
			if (file.list().length == 0) {

//				file.delete();
				logger.info("### NO FILE FOUND IN THIS DIRECTORY : " + file.getAbsolutePath());

			} else {
				
				for(File file1: file.listFiles()) {
				    if (!file1.isDirectory()) { 
				        file1.delete();
				        logger.info("### GENERATED FILE DELETED FROM FILES FOLDER : " + file.getAbsolutePath());
				    }
				}
				
//				// list all the directory contents
//				String files[] = file.list();
//
//				for (String temp : files) {
//					// construct the file structure
//					File fileDelete = new File(file, temp);
//
//					// recursive delete
//					delete(fileDelete);
//				}

				
				
				// check the directory again, if empty then delete it
//				if (file.list().length == 0) {
//					file.delete();
//					logger.info("Directory is deleted : " + file.getAbsolutePath());
//				}
				
				
			}

		} else {
			// if file, then delete it
			file.delete();
			logger.info("### FILE IS DELETED : " + file.getAbsolutePath());
		}
	}
}