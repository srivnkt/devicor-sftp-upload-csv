package com.java.devicor.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author VSEERLA
 *
 */
public class DevicorUtil {

	private final static Logger logger = LoggerFactory.getLogger(DevicorUtil.class);

	public static String countryCanada = "\'Canada\'";
	public static String russia = "\'Russia\'";
	public static String china = "\'China\'";
	public static String unitedStates = "\'United States\'";
	public static String australia = "\'Australia\'";

	/**
	 * 
	 * @return
	 */
	public static String getLocalDateTime() {

		System.out.println("###  DevicorUtil.getLocalDateTime() :: START");
		float noOfDays = getNoOfDays();
		logger.info("### NO OF DAYS ::: "+noOfDays);
		int round = Math.round(noOfDays);

		DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, -round);
		now.set(Calendar.HOUR, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.HOUR_OF_DAY, 0);

		LocalDateTime ldt = LocalDateTime.ofInstant(now.getTime().toInstant(), ZoneId.systemDefault());
		ZonedDateTime zonedDateTime = ZonedDateTime.of(ldt, ZoneId.of("UTC"));
		String date = zonedDateTime.format(DATE_TIME_FORMATTER);
		date = date + "Z";
		logger.info("### YESTERDAY DAY DATE IS : " + date);
		logger.info("###  DevicorUtil.getLocalDateTime() :: END");
		return date;
	}
	
	
	/**
	 * 
	 * @return
	 */
	public static String getLastModifiedDate() {
		
		logger.info("###  DevicorUtil.getLastModifiedDate() :: START");
		float noOfDays = getNoOfDays();
		logger.info("### NO OF DAYS ::: "+noOfDays);
		int round = Math.round(noOfDays);
		
		DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, -round);
		now.set(Calendar.HOUR, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.HOUR_OF_DAY, 0);

		LocalDateTime ldt = LocalDateTime.ofInstant(now.getTime().toInstant(), ZoneId.systemDefault());
		ZonedDateTime zonedDateTime = ZonedDateTime.of(ldt, ZoneId.of("UTC"));
		String date = zonedDateTime.format(DATE_TIME_FORMATTER);
		date = date + "Z";
		logger.info("### YESTERDAY DAY DATE IS : " + date);
		logger.info("###  DevicorUtil.getLastModifiedDate() :: END");
		return date;
	}
	
	/**
	 * 
	 */
	public void getCurrentDateTime() {

		logger.info("###  DevicorUtil.getCurrentDateTime() :: START");

		DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		Calendar now = Calendar.getInstance();
		now.add(Calendar.DATE, -1);
		now.set(Calendar.HOUR, 0);
		now.set(Calendar.MINUTE, 0);
		now.set(Calendar.SECOND, 0);
		now.set(Calendar.HOUR_OF_DAY, 0);

		LocalDateTime ldt = LocalDateTime.ofInstant(now.getTime().toInstant(), ZoneId.systemDefault());
		ZonedDateTime zonedDateTime = ZonedDateTime.of(ldt, ZoneId.of("UTC"));
		String currentDateTime = zonedDateTime.format(DATE_TIME_FORMATTER);
		logger.info("### CREATED DATE IS : " + currentDateTime);
		
		DevicorUtil devicorUtil = new DevicorUtil();
		Properties prop = devicorUtil.loadPropertiesFile("application.properties");
		String path = prop.getProperty("path");
		File directory = new File(path);
		
		if (!directory.exists()) {

			try {
	            FileUtils.writeStringToFile(new File(path), currentDateTime, StandardCharsets.UTF_8);
	        } catch (IOException e) {
	           logger.info("### EXCEPTION OCCURED :: "+e.getMessage());
	        }
		}
		logger.info("###  DevicorUtil.getCurrentDateTime() :: END");
	}
	
	/**
	 * 
	 * @return
	 */
	public static float getNoOfDays() {

		logger.info("###  DevicorUtil.getNoOfDays() :: START");
		
		DevicorUtil devicorUtil = new DevicorUtil();
		Properties prop = devicorUtil.loadPropertiesFile("application.properties");
		String path = prop.getProperty("path");
		File file = new File(path);
		String dateBeforeString = null;
		
		if (file.exists()) {

			try {
				
				dateBeforeString = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
				logger.info("### DATE BEFORE STRING : " + file.lastModified());
	        } catch (IOException e) {
	           logger.info("### EXCEPTION OCCURED :: "+e.getMessage());
	        }
		}else {
			
			DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
			Calendar now = Calendar.getInstance();
			now.add(Calendar.DATE, -1);
			now.set(Calendar.HOUR, 0);
			now.set(Calendar.MINUTE, 0);
			now.set(Calendar.SECOND, 0);
			now.set(Calendar.HOUR_OF_DAY, 0);

			LocalDateTime ldt = LocalDateTime.ofInstant(now.getTime().toInstant(), ZoneId.systemDefault());
			ZonedDateTime zonedDateTime = ZonedDateTime.of(ldt, ZoneId.of("UTC"));
			dateBeforeString = zonedDateTime.format(DATE_TIME_FORMATTER);
			logger.info("### CREATED DATE IS : " + dateBeforeString);
			
		}
		
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		logger.info("### DATE AFTER FORMAT : " + sdf.format(file.lastModified()));
		
		Date date = new Date();
		String dateAfterString = sdf.format(date);
		logger.info("### CONVERTING DATE TO STRING ::: " + dateAfterString);
		float daysBetween = 0.f;
		
		try {
		       Date dateBefore = sdf.parse(dateBeforeString);
		       Date dateAfter = sdf.parse(dateAfterString);
		       long difference = dateAfter.getTime() - dateBefore.getTime();
		       daysBetween = (difference / (1000*60*60*24));
		       logger.info("Number of Days between dates: "+daysBetween);
		       
		 } catch (Exception e) {
		       logger.error("EXCEPTION OCCURED:"+e.getMessage());
		 }
		
		logger.info("###  DevicorUtil.getNoOfDays() :: END");
		return daysBetween;
	}

	/**
	 * 
	 * @param country
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	public static String getUSCountryCode(String country) throws UnsupportedEncodingException {
		
		
		String usCountryNames[] = { "\'Algeria\'", "\'Angola\'", "\'Botswana\'", "\'Burundi\'", "\'Cameroon\'",
				"\'Cape Verde\'", "\'Central African Republic\'", "\'Chad\'", "\'Comoros\'", "\'Mayotte\'",
				"\'Congo\'", "\'Benin\'", "\'Equatorial Guinea\'", "\'Ethiopia\'",
				"\'Eritrea\'", "\'Djibouti\'", "\'Gabon\'", "\'Gambia\'", "\'Ghana\'", "\'Guinea\'",
				"\'Kenya\'", "\'Lesotho\'", "\'Liberia\'", "\'Libya\'", "\'Madagascar\'",
				"\'Malawi\'", "\'Mali\'", "\'Mauritania\'", "\'Mauritius\'", "\'Morocco\'", "\'Mozambique\'",
				"\'Namibia\'", "\'Niger\'", "\'Nigeria\'", "\'Guinea-Bissau\'", "\'Reunion\'", "\'Rwanda\'",
				"\'Saint Helena\'", "\'Senegal\'", "\'Seychelles\'", "\'Sierra Leone\'",
				"\'Somalia\'", "\'South Africa\'", "\'Zimbabwe\'", "\'South Sudan\'", "\'Sudan\'", "\'Western Sahara\'",
				"\'Swaziland\'", "\'Togo\'", "\'Tunisia\'", "\'Uganda\'", "\'Egypt\'", "\'Tanzania\'",
				"\'Burkina Faso\'", "\'Zambia\'", "\'Antarctica\'", "\'Bouvet Island \'",
				"\'South Georgia and the South Sandwich Islands\'", "\'French Southern Territories\'",
				"\'Heard Island and McDonald Islands\'", "\'Antigua and Barbuda\'", "\'Bahamas\'", "\'Barbados\'",
				"\'Bermuda\'", "\'Belize\'", "\'British Virgin Islands\'", "\'Canada\'", "\'Cayman Islands\'",
				"\'Costa Rica\'", "\'Cuba\'", "\'Dominica\'", "\'El Salvador\'", "\'Greenland\'", "\'Grenada\'",
				"\'Guadeloupe\'", "\'Guatemala\'", "\'Haiti\'", "\'Honduras\'", "\'Jamaica\'", "\'Martinique\'",
				"\'Mexico\'", "\'Montserrat\'", "\'Netherlands Antilles\'", "\'Curaçao\'", "\'Aruba\'",
				"\'Bonaire\'", "\'Nicaragua\'",
				"\'United States Minor Outlying Islands\'", "\'Panama\'", "\'Puerto Rico\'", "\'Saint Barthelemy\'",
				"\'Saint Kitts and Nevis\'", "\'Anguilla\'", "\'Saint Lucia\'", "\'Saint Martin\'",
				"\'Saint Pierre and Miquelon\'", "\'Saint Vincent and the Grenadines\'", "\'Trinidad and Tobago\'",
				"\'Turks and Caicos Islands\'", "\'United States of America\'", "\'United States Virgin Islands\'",
				"\'Guam\'", "\'United States Minor Outlying Islands\'", "\'Argentina\'", "\'Bolivia\'", "\'Brazil\'",
				"\'Chile\'", "\'Colombia\'", "\'Ecuador\'", "\'Falkland Islands\'", "\'French Guiana\'", "\'Guyana\'",
				"\'Paraguay\'", "\'Peru\'", "\'Suriname\'", "\'Uruguay\'", "\'Venezuela\'"
				
		};
		
		StringBuilder sb = new StringBuilder();
		for (String string : usCountryNames) {
			sb.append(string).append(",");
		}
		
		String sb1 = sb.toString();
		sb1 = sb1.substring(0, sb1.length()-1);
		
//		String sb2 = URLEncoder.encode(sb1,"UTF-8");
		
		country = "\'" + country + "\'";
//		return country + "," + sb1 + ")";
		
		return country + "," + sb1 + ") AND CreatedDate>=" + DevicorUtil.getLocalDateTime()
		+ " AND LastModifiedDate>=" + DevicorUtil.getLastModifiedDate();

//		country = "\'" + country + "\'";
//		return country + "," + countryCanada + ") AND CreatedDate>=" + DevicorUtil.getLocalDateTime()
//				+ " AND LastModifiedDate>" + DevicorUtil.getLocalDateTime();
	}
	
	
	
	
	public static String getUSCountryCode1(String country) throws UnsupportedEncodingException {
		
		
		String usCountryNames[] = { "\'Algeria\'", "\'Angola\'", "\'Botswana\'", "\'Burundi\'", "\'Cameroon\'",
				"\'Cape Verde\'", "\'Central African Republic\'", "\'Chad\'", "\'Comoros\'", "\'Mayotte\'",
				"\'Congo\'", "\'Benin\'", "\'Equatorial Guinea\'", "\'Ethiopia\'",
				"\'Eritrea\'", "\'Djibouti\'", "\'Gabon\'", "\'Gambia\'", "\'Ghana\'", "\'Guinea\'",
				"\'Kenya\'", "\'Lesotho\'", "\'Liberia\'", "\'Libya\'", "\'Madagascar\'",
				"\'Malawi\'", "\'Mali\'", "\'Mauritania\'", "\'Mauritius\'", "\'Morocco\'", "\'Mozambique\'",
				"\'Namibia\'", "\'Niger\'", "\'Nigeria\'", "\'Guinea-Bissau\'", "\'Reunion\'", "\'Rwanda\'",
				"\'Saint Helena\'", "\'Senegal\'", "\'Seychelles\'", "\'Sierra Leone\'",
				"\'Somalia\'", "\'South Africa\'", "\'Zimbabwe\'", "\'South Sudan\'", "\'Sudan\'", "\'Western Sahara\'",
				"\'Swaziland\'", "\'Togo\'", "\'Tunisia\'", "\'Uganda\'", "\'Egypt\'", "\'Tanzania\'",
				"\'Burkina Faso\'", "\'Zambia\'", "\'Antarctica\'", "\'Bouvet Island \'",
				"\'South Georgia and the South Sandwich Islands\'", "\'French Southern Territories\'",
				"\'Heard Island and McDonald Islands\'", "\'Antigua and Barbuda\'", "\'Bahamas\'", "\'Barbados\'",
				"\'Bermuda\'", "\'Belize\'", "\'British Virgin Islands\'", "\'Canada\'", "\'Cayman Islands\'",
				"\'Costa Rica\'", "\'Cuba\'", "\'Dominica\'", "\'El Salvador\'", "\'Greenland\'", "\'Grenada\'",
				"\'Guadeloupe\'", "\'Guatemala\'", "\'Haiti\'", "\'Honduras\'", "\'Jamaica\'", "\'Martinique\'",
				"\'Mexico\'", "\'Montserrat\'", "\'Netherlands Antilles\'", "\'Curaçao\'", "\'Aruba\'",
				"\'Bonaire\'", "\'Nicaragua\'",
				"\'United States Minor Outlying Islands\'", "\'Panama\'", "\'Puerto Rico\'", "\'Saint Barthelemy\'",
				"\'Saint Kitts and Nevis\'", "\'Anguilla\'", "\'Saint Lucia\'", "\'Saint Martin\'",
				"\'Saint Pierre and Miquelon\'", "\'Saint Vincent and the Grenadines\'", "\'Trinidad and Tobago\'",
				"\'Turks and Caicos Islands\'", "\'United States of America\'", "\'United States Virgin Islands\'",
				"\'Guam\'", "\'United States Minor Outlying Islands\'", "\'Argentina\'", "\'Bolivia\'", "\'Brazil\'",
				"\'Chile\'", "\'Colombia\'", "\'Ecuador\'", "\'Falkland Islands\'", "\'French Guiana\'", "\'Guyana\'",
				"\'Paraguay\'", "\'Peru\'", "\'Suriname\'", "\'Uruguay\'", "\'Venezuela\'"
				
		};
		
		StringBuilder sb = new StringBuilder();
		for (String string : usCountryNames) {
			sb.append(string).append(",");
		}
		
		String sb1 = sb.toString();
		sb1 = sb1.substring(0, sb1.length()-1);
		
		country = "\'" + country + "\'";
		return country + "," + sb1 + ")";
	}
	
	

	/**
	 * 
	 * @param country
	 * @return
	 */
	public static String getEUCountryCode(String country) {

		
		String euCountryNames[] = { "\'Azerbaijan\'", "\'Armenia\'", "\'Cyprus\'", "\'Georgia\'", "\'Kazakhstan\'",
				"\'Turkey\'", "\'Albania\'", "\'Andorra\'", "\'Austria\'", "\'Armenia\'", "\'Belgium\'",
				"\'Bosnia and Herzegovina\'", "\'Bulgaria\'", "\'Belarus\'", "\'Croatia\'", "\'Cyprus\'",
				"\'Czech Republic\'", "\'Denmark\'", "\'Estonia\'", "\'Faroe Islands\'", "\'Finland\'",
				"\'Åland Islands\'", "\'France\'", "\'Georgia\'", "\'Germany\'", "\'Gibraltar\'", "\'Greece\'",
				"\'Vatican City State\'", "\'Hungary\'", "\'Iceland\'", "\'Ireland\'", "\'Italy\'",
				"\'Kazakhstan\'", "\'Latvia\'", "\'Liechtenstein\'", "\'Lithuania\'", "\'Luxembourg\'", "\'Malta\'",
				"\'Monaco\'", "\'Moldova\'", "\'Montenegro\'", "\'Netherlands\'", "\'Norway\'", "\'Poland\'",
				"\'Portugal\'", "\'Romania\'", "\'San Marino\'", "\'Serbia\'", "\'Slovakia \'", "\'Slovenia\'",
				"\'Spain\'", "\'Jan Mayen Islands\'", "\'Sweden\'", "\'Switzerland\'", "\'Turkey\'",
				"\'Ukraine\'", "\'Macedonia\'", "\'United Kingdom\'", "\'Guernsey\'", "\'Jersey\'", "\'Isle of Man\'","\'UK\'" };
		
		StringBuilder sb = new StringBuilder();
		for (String string : euCountryNames) {
			sb.append(string).append(",");
		}
		
		String sb1 = sb.toString();
		sb1 = sb1.substring(0, sb1.length()-1);
		
		return sb1.toString()+ ") AND createdDate>="
				+ DevicorUtil.getLocalDateTime() + " AND LastModifiedDate >=" + DevicorUtil.getLastModifiedDate();
		
//		return sb1.toString()+ ")";
//		country = "\'" + country + "\'";
//		return countryCanada + "," + russia + "," + china + "," + unitedStates + "," + australia + ") AND createdDate>="
//				+ DevicorUtil.getLocalDateTime() + " AND LastModifiedDate >" + DevicorUtil.getLocalDateTime();
		
	}
	
	
	
	/**
	 * 
	 * @param country
	 * @return
	 */
	public static String getEUCountryCode1(String country) {

		
		String euCountryNames[] = { "\'Azerbaijan\'", "\'Armenia\'", "\'Cyprus\'", "\'Georgia\'", "\'Kazakhstan\'",
				"\'Turkey\'", "\'Albania\'", "\'Andorra\'", "\'Austria\'", "\'Armenia\'", "\'Belgium\'",
				"\'Bosnia and Herzegovina\'", "\'Bulgaria\'", "\'Belarus\'", "\'Croatia\'", "\'Cyprus\'",
				"\'Czech Republic\'", "\'Denmark\'", "\'Estonia\'", "\'Faroe Islands\'", "\'Finland\'",
				"\'Åland Islands\'", "\'France\'", "\'Georgia\'", "\'Germany\'", "\'Gibraltar\'", "\'Greece\'",
				"\'Vatican City State\'", "\'Hungary\'", "\'Iceland\'", "\'Ireland\'", "\'Italy\'",
				"\'Kazakhstan\'", "\'Latvia\'", "\'Liechtenstein\'", "\'Lithuania\'", "\'Luxembourg\'", "\'Malta\'",
				"\'Monaco\'", "\'Moldova\'", "\'Montenegro\'", "\'Netherlands\'", "\'Norway\'", "\'Poland\'",
				"\'Portugal\'", "\'Romania\'", "\'San Marino\'", "\'Serbia\'", "\'Slovakia \'", "\'Slovenia\'",
				"\'Spain\'", "\'Jan Mayen Islands\'", "\'Sweden\'", "\'Switzerland\'", "\'Turkey\'",
				"\'Ukraine\'", "\'Macedonia\'", "\'United Kingdom\'", "\'Guernsey\'", "\'Jersey\'", "\'Isle of Man\'","\'UK\'" };
		
		StringBuilder sb = new StringBuilder();
		for (String string : euCountryNames) {
			sb.append(string).append(",");
		}
		
		String sb1 = sb.toString();
		sb1 = sb1.substring(0, sb1.length()-1);
		
		return sb1.toString()+ ")";
		
	}
	
	

	/**
	 * 
	 * @param country
	 * @return
	 */
	public static String getAUCountryCode(String country) {
		
		country =	"\'Australia\'";
		
		String auCountryName[] = { "\'Afghanistan\'", "\'Bahrain\'", "\'Bangladesh\'", "\'Bhutan\'",
				"\'British Indian Ocean Territory\'", "\'Brunei Darussalam\'", "\'Myanmar\'", "\'Cambodia\'",
				"\'Sri Lanka\'", "\'Taiwan\'", "\'Christmas Island\'", "\'Cocos Islands\'", "\'Palestine\'",
				"\'Hong Kong\'", "\'India\'", "\'Indonesia\'", "\'Iran\'", "\'Iraq\'", "\'Israel\'", "\'Japan\'",
				"\'Jordan\'", "\'Korea\'", "\'North Korea\'", "\'South Korea\'", "\'Kuwait\'", "\'Kyrgyz \'",
				"\'Lao\'", "\'Lebanon\'", "\'Macao\'", "\'Malaysia\'", "\'Maldives\'",
				"\'Mongolia\'", "\'Oman\'", "\'Nepal\'", "\'Pakistan\'", "\'Philippines\'", "\'Timor-Leste\'",
				"\'Qatar\'", "\'Saudi Arabia\'", "\'Singapore\'", "\'Vietnam\'", "\'Syrian Arab Republic\'",
				"\'Tajikistan\'", "\'Thailand\'", "\'United Arab Emirates\'", "\'Turkmenistan\'", "\'Uzbekistan\'",
				"\'Yemen\'", "\'American Samoa\'", "\'Australia\'", "\'Solomon Islands\'", "\'Cook Islands\'",
				"\'Fiji\'", "\'French Polynesia\'", "\'Kiribati\'", "\'Nauru\'", "\'New Caledonia\'", "\'Vanuatu\'",
				"\'New Zealand\'", "\'Niue\'", "\'Norfolk Island\'", "\'Northern Mariana Islands\'", "\'Micronesia\'",
				"\'Marshall Islands\'", "\'Palau\'", "\'Papua New Guinea\'", "\'Pitcairn Islands\'", "\'Tokelau\'",
				"\'Tonga\'", "\'Tuvalu\'", "\'Wallis and Futuna\'", "\'Samoa\'" };
		
		StringBuilder sb = new StringBuilder();
		for (String string : auCountryName) {
			sb.append(string).append(",");
		}
		
		String sb1 = sb.toString();
		sb1 = sb1.substring(0, sb1.length()-1);

//		return country + "," +sb1.toString()+ ")";
		
//		country = "\'" + country + "\'";
//		String korea = "\'Korea\'";
//		String japan = "\'Japan\'";
//		return country + "," + korea + "," + japan + ") AND createdDate>=" + DevicorUtil.getLocalDateTime()
//				+ " AND LastModifiedDate >" + DevicorUtil.getLocalDateTime();

//		return country + "," +sb1.toString()+ ")";
		
		return country + "," +sb1.toString()+ ") AND createdDate>=" + DevicorUtil.getLocalDateTime()
		+ " AND LastModifiedDate >=" + DevicorUtil.getLastModifiedDate();
		
	}
	
	
	
	/**
	 * 
	 * @param country
	 * @return
	 */
	public static String getAUCountryCode1(String country) {
		
		country =	"\'Australia\'";
		
		String auCountryName[] = { "\'Afghanistan\'", "\'Bahrain\'", "\'Bangladesh\'", "\'Bhutan\'",
				"\'British Indian Ocean Territory\'", "\'Brunei Darussalam\'", "\'Myanmar\'", "\'Cambodia\'",
				"\'Sri Lanka\'", "\'Taiwan\'", "\'Christmas Island\'", "\'Cocos Islands\'", "\'Palestine\'",
				"\'Hong Kong\'", "\'India\'", "\'Indonesia\'", "\'Iran\'", "\'Iraq\'", "\'Israel\'", "\'Japan\'",
				"\'Jordan\'", "\'Korea\'", "\'North Korea\'", "\'South Korea\'", "\'Kuwait\'", "\'Kyrgyz \'",
				"\'Lao\'", "\'Lebanon\'", "\'Macao\'", "\'Malaysia\'", "\'Maldives\'",
				"\'Mongolia\'", "\'Oman\'", "\'Nepal\'", "\'Pakistan\'", "\'Philippines\'", "\'Timor-Leste\'",
				"\'Qatar\'", "\'Saudi Arabia\'", "\'Singapore\'", "\'Vietnam\'", "\'Syrian Arab Republic\'",
				"\'Tajikistan\'", "\'Thailand\'", "\'United Arab Emirates\'", "\'Turkmenistan\'", "\'Uzbekistan\'",
				"\'Yemen\'", "\'American Samoa\'", "\'Australia\'", "\'Solomon Islands\'", "\'Cook Islands\'",
				"\'Fiji\'", "\'French Polynesia\'", "\'Kiribati\'", "\'Nauru\'", "\'New Caledonia\'", "\'Vanuatu\'",
				"\'New Zealand\'", "\'Niue\'", "\'Norfolk Island\'", "\'Northern Mariana Islands\'", "\'Micronesia\'",
				"\'Marshall Islands\'", "\'Palau\'", "\'Papua New Guinea\'", "\'Pitcairn Islands\'", "\'Tokelau\'",
				"\'Tonga\'", "\'Tuvalu\'", "\'Wallis and Futuna\'", "\'Samoa\'" };
		
		StringBuilder sb = new StringBuilder();
		for (String string : auCountryName) {
			sb.append(string).append(",");
		}
		
		String sb1 = sb.toString();
		sb1 = sb1.substring(0, sb1.length()-1);

		return country + "," +sb1.toString()+ ")";
		
	}
	

	/**
	 * 
	 * @param country
	 * @return
	 */
	public static String getRUCountryCode(String country) {

		country = "\'" + country + "\'";
//		String country2 = country+")";
		return country + ") AND createdDate>=" + DevicorUtil.getLocalDateTime() + " AND LastModifiedDate >="
				+ DevicorUtil.getLastModifiedDate();
	}
	
	
	/**
	 * 
	 * @param country
	 * @return
	 */
	public static String getRUCountryCode1(String country) {

		country = "\'" + country + "\'";
		return country + ")";
	}
	

	/**
	 * 
	 * @param country
	 * @return
	 */
	public static String getCNCountryCode(String country) {

		country = "\'" + country + "\'";
//		String country2 = country+")";
		return country + ") AND createdDate>=" + DevicorUtil.getLocalDateTime() + " AND LastModifiedDate >="
				+ DevicorUtil.getLastModifiedDate();
	}

	
	/**
	 * 
	 * @param country
	 * @return
	 */
	public static String getCNCountryCode1(String country) {

		country = "\'" + country + "\'";
		return country + ")";
	}
	
	/**
	 * 
	 * @param country
	 * @return
	 */
	public static boolean searchUSCountryName(String country) {
		
		boolean flag = false;
		
		String[] usCountryName = {"United States","Algeria", "Angola", "Botswana", "Burundi", "Cameroon",
				"Cape Verde", "Central African Republic", "Chad", "Comoros", "Mayotte",
				"Congo", "Benin", "Equatorial Guinea", "Ethiopia",
				"Eritrea", "Djibouti", "Gabon", "Gambia", "Ghana", "Guinea",
				"Kenya", "Lesotho", "Liberia", "Libya", "Madagascar",
				"Malawi", "Mali", "Mauritania", "Mauritius", "Morocco", "Mozambique",
				"Namibia", "Niger", "Nigeria", "Guinea-Bissau", "Reunion", "Rwanda",
				"Saint Helena", "Senegal", "Seychelles", "Sierra Leone",
				"Somalia", "South Africa", "Zimbabwe", "South Sudan", "Sudan", "Western Sahara",
				"Swaziland", "Togo", "Tunisia", "Uganda", "Egypt", "Tanzania",
				"Burkina Faso", "Zambia", "Antarctica", "Bouvet Island ",
				"South Georgia and the South Sandwich Islands", "French Southern Territories",
				"Heard Island and McDonald Islands", "Antigua and Barbuda", "Bahamas", "Barbados",
				"Bermuda", "Belize", "British Virgin Islands", "Canada", "Cayman Islands",
				"Costa Rica", "Cuba", "Dominica", "El Salvador", "Greenland", "Grenada",
				"Guadeloupe", "Guatemala", "Haiti", "Honduras", "Jamaica", "Martinique",
				"Mexico", "Montserrat", "Netherlands Antilles", "Curaçao", "Aruba",
				"Bonaire", "Nicaragua",
				"United States Minor Outlying Islands", "Panama", "Puerto Rico", "Saint Barthelemy",
				"Saint Kitts and Nevis", "Anguilla", "Saint Lucia", "Saint Martin",
				"Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Trinidad and Tobago",
				"Turks and Caicos Islands", "United States of America", "United States Virgin Islands",
				"Guam", "United States Minor Outlying Islands", "Argentina", "Bolivia", "Brazil",
				"Chile", "Colombia", "Ecuador", "Falkland Islands", "French Guiana", "Guyana",
				"Paraguay", "Peru", "Suriname", "Uruguay", "Venezuela"};

				for (String  usCountry : usCountryName) {
					
					if(country.equals(usCountry)) {
						flag = true;
						break;
					}
				}
		       
		return flag;
	}
	
	/**
	 * 
	 * @param country
	 * @return
	 */
	public static boolean searchAUCountryName(String country) {
		
		boolean flag = false;
		
		String[] auCountryName = {"Australia","Afghanistan", "Bahrain", "Bangladesh", "Bhutan",
				"British Indian Ocean Territory", "Brunei Darussalam", "Myanmar", "Cambodia",
				"Sri Lanka", "Taiwan", "Christmas Island", "Cocos Islands", "Palestine",
				"Hong Kong", "India", "Indonesia", "Iran", "Iraq", "Israel", "Japan",
				"Jordan", "Korea", "North Korea", "South Korea", "Kuwait", "Kyrgyz ",
				"Lao", "Lebanon", "Macao", "Malaysia", "Maldives",
				"Mongolia", "Oman", "Nepal", "Pakistan", "Philippines", "Timor-Leste",
				"Qatar", "Saudi Arabia", "Singapore", "Vietnam", "Syrian Arab Republic",
				"Tajikistan", "Thailand", "United Arab Emirates", "Turkmenistan", "Uzbekistan",
				"Yemen", "American Samoa", "Australia", "Solomon Islands", "Cook Islands",
				"Fiji", "French Polynesia", "Kiribati", "Nauru", "New Caledonia", "Vanuatu",
				"New Zealand", "Niue", "Norfolk Island", "Northern Mariana Islands", "Micronesia",
				"Marshall Islands", "Palau", "Papua New Guinea", "Pitcairn Islands", "Tokelau",
				"Tonga", "Tuvalu", "Wallis and Futuna", "Samoa"};

				for (String  auCountry : auCountryName) {
					
					if(country.equals(auCountry)) {
						flag = true;
						break;
					}
				}
		
		return flag;
	}
	
	
	/**
	 * 
	 * @param country
	 * @return
	 */
	public static boolean searchEUCountryName(String country) {
		
		boolean flag = false;
		
		String[] euCountryName = {"Global","Azerbaijan", "Armenia", "Cyprus", "Georgia", "Kazakhstan",
				"Turkey", "Albania", "Andorra", "Austria", "Armenia", "Belgium",
				"Bosnia and Herzegovina", "Bulgaria", "Belarus", "Croatia", "Cyprus",
				"Czech Republic", "Denmark", "Estonia", "Faroe Islands", "Finland",
				"Åland Islands", "France", "Georgia", "Germany", "Gibraltar", "Greece",
				"Vatican City State", "Hungary", "Iceland", "Ireland", "Italy",
				"Kazakhstan", "Latvia", "Liechtenstein", "Lithuania", "Luxembourg", "Malta",
				"Monaco", "Moldova", "Montenegro", "Netherlands", "Norway", "Poland",
				"Portugal", "Romania", "San Marino", "Serbia", "Slovakia ", "Slovenia",
				"Spain", "Jan Mayen Islands", "Sweden", "Switzerland", "Turkey",
				"Ukraine", "Macedonia", "United Kingdom", "Guernsey", "Jersey", "Isle of Man","UK"};

				for (String  euCountry : euCountryName) {
					
					if(country.equals(euCountry)) {
						flag = true;
						break;
					}
				}
		
		return flag;
	}
	
	/**
	 * 
	 * @param filePath
	 * @return
	 */
	public Properties loadPropertiesFile(String filePath) {

        Properties prop = new Properties();

        try {
        	
        	InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(filePath);
            prop.load(resourceAsStream);
        } catch (IOException e) {
        	logger.error("Unable to load properties file : " + filePath);
        }

        return prop;
    }
	
//	public static void main(String[] args) {
//		DevicorUtil d = new DevicorUtil();
//		d.getCurrentDateTime();
//		System.out.println(d.getNoOfDays());
//		getLastModifiedDate();
//	}
	
}
